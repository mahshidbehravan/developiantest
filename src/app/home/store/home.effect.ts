
import { map, mergeAll, switchMap, tap, catchError, delay } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Action, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import * as homeAction from './home.action';
import { ClassMockService } from '../../services/class.service.mock';
import { ClassModel } from '../../models/class';
import { StudentMockService } from 'src/app/services/student.mock';


@Injectable()
export class HomeEffects {

    constructor(private actions$: Actions,
        private classMockService: ClassMockService,
        private studentMockService: StudentMockService
    ) {
    }
    @Effect()
    getClasses$: Observable<Action> = this.actions$.pipe(
        ofType(homeAction.ActionTypes.Get_Class),
        map((action: homeAction.GetClassAction) => action.payload),
        switchMap(() => this.classMockService.get()),
        map(res =>  {
            return  new homeAction.GetClassListAction(res );
        })
 );

    @Effect()
    editClasses$: Observable<Action> = this.actions$.pipe(
        ofType(homeAction.ActionTypes.Edit_Class),
        map((action: homeAction.EditClassAction) => action.payload),
        switchMap((classModel) => this.classMockService.edit(classModel)),
        map(res => {
             return  new homeAction.GetClassAction();
            })
    );

    @Effect()
    addClasses$: Observable<Action> = this.actions$.pipe(
        ofType(homeAction.ActionTypes.Add_Class),
        map((action: homeAction.AddClassAction) => action.payload),
        switchMap((classModel) => this.classMockService.add(classModel)),
        map(res => {
             return  new homeAction.GetClassAction();
            })
    );

    @Effect()
    deleteClasses$: Observable<Action> = this.actions$.pipe(
        ofType(homeAction.ActionTypes.Delete_Class),
        map((action: homeAction.DeleteClassAction) => action.payload),
        switchMap((classModel) => this.classMockService.delete(classModel)),
        map(res => {
             return  new homeAction.GetClassAction();
            })
    );

    @Effect()
    getStudent$: Observable<Action> = this.actions$.pipe(
        ofType(homeAction.ActionTypes.Get_Student),
        map((action: homeAction.GetStudentAction) => action.payload),
        switchMap((classId) => this.studentMockService.getStudent(classId)),
        map(res => {
             return  new homeAction.GetStudentListAction(res);
            })
    );

    @Effect()
    addStudent$: Observable<Action> = this.actions$.pipe(
        ofType(homeAction.ActionTypes.Add_Student),
        map((action: homeAction.AddStudentAction) => action.payload),
        switchMap((student) => this.studentMockService.add(student)),
        map(res => {
             return  new homeAction.GetStudentAction(res);
            })
    );

    @Effect()
    editStudent$: Observable<Action> = this.actions$.pipe(
        ofType(homeAction.ActionTypes.Edit_Student),
        map((action: homeAction.EditStudentAction) => action.payload),
        switchMap((student) => this.studentMockService.edit(student)),
        map(res => {
             return  new homeAction.GetStudentAction(res);
            })
    );
    @Effect()
    deleteStudent$: Observable<Action> = this.actions$.pipe(
        ofType(homeAction.ActionTypes.Delete_Student),
        map((action: homeAction.DeleteStudentAction) => action.payload),
        switchMap((student) => this.studentMockService.delete(student)),
        map(res => {
             return  new homeAction.GetStudentAction(res);
            })
    );

}


