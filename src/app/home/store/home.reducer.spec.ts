
import * as fromReducer from './home.reducer';
import * as  fromAction from './home.action';

describe('Reducers', () => {
  it('Get_Class', () => {
    const { INITIAL_STATE } = fromReducer;
    const action = new fromAction.GetClassAction();
    const state = fromReducer.reducer(INITIAL_STATE, action);
    expect(state.ClassData).toEqual(null);
  });
  it('Get_Class_List', () => {
    const { INITIAL_STATE } = fromReducer;
    const action = new fromAction.GetClassListAction([]);
    const state = fromReducer.reducer(INITIAL_STATE, action);
    expect(state.ClassList).toEqual([]);
  });
  it('Edit_Class', () => {
    const { INITIAL_STATE } = fromReducer;
    const action = new fromAction.EditClassAction(null);
    const state = fromReducer.reducer(INITIAL_STATE, action);
    expect(state.EditClass).toEqual(null);
  });
  it('Add_Class', () => {
    const { INITIAL_STATE } = fromReducer;
    const action = new fromAction.AddClassAction(null);
    const state = fromReducer.reducer(INITIAL_STATE, action);
    expect(state.AddClass).toEqual(null);
  });
  it('Delete_Class', () => {
    const { INITIAL_STATE } = fromReducer;
    const action = new fromAction.DeleteClassAction(null);
    const state = fromReducer.reducer(INITIAL_STATE, action);
    expect(state.DeleteClass).toEqual(null);
  });
  it('Selected_Class', () => {
    const { INITIAL_STATE } = fromReducer;
    const action = new fromAction.SelectedClassAction(null);
    const state = fromReducer.reducer(INITIAL_STATE, action);
    expect(state.SelectedClass).toEqual(null);
  });
  it('Get_Student', () => {
    const { INITIAL_STATE } = fromReducer;
    const action = new fromAction.GetStudentAction(null);
    const state = fromReducer.reducer(INITIAL_STATE, action);
    expect(state.StudentData).toEqual(null);
  });
  it('Get_Student_List', () => {
    const { INITIAL_STATE } = fromReducer;
    const action = new fromAction.GetStudentListAction([]);
    const state = fromReducer.reducer(INITIAL_STATE, action);
    expect(state.StudentList).toEqual([]);
  });
  it('Add_Student', () => {
    const { INITIAL_STATE } = fromReducer;
    const action = new fromAction.AddStudentAction(null);
    const state = fromReducer.reducer(INITIAL_STATE, action);
    expect(state.AddStudent).toEqual(null);
  });
  it('Edit_Student', () => {
    const { INITIAL_STATE } = fromReducer;
    const action = new fromAction.EditStudentAction(null);
    const state = fromReducer.reducer(INITIAL_STATE, action);
    expect(state.EditStudent).toEqual(null);
  });
  it('Delete_Student', () => {
    const { INITIAL_STATE } = fromReducer;
    const action = new fromAction.DeleteStudentAction(null);
    const state = fromReducer.reducer(INITIAL_STATE, action);
    expect(state.DeleteStudent).toEqual(null);
  });

});
