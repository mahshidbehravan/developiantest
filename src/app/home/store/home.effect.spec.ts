import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { hot, cold } from 'jasmine-marbles';
import { Observable, Subject, ReplaySubject } from 'rxjs';

import { HomeEffects } from './home.effect';
import * as HomeActions from './home.action';
import { GetClassAction, GetClassListAction, GetStudentAction } from './home.action';
import { HomeModule } from '../home.module';
import { getEffectsMetadata, EffectsMetadata, EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { reducers } from 'src/app';
import { switchMap } from 'rxjs/operators';
import { ClassMockService } from 'src/app/services/class.service.mock';

describe('Effects', () => {
  let effects: HomeEffects;
  let actions: Observable<any>;
 let metadata: EffectsMetadata<HomeEffects>;

let classes = [
  { id: 1, className: 'Biology', location: 'Building 3 Room 201', teacher: 'Mr Robertson' },
  { id: 2, className: 'English', location: 'Building 3 Room 134', teacher: 'Miss Sanderson' },
];

let oneClass= { id: 1, className: 'Biology', location: 'Building 3 Room 201', teacher: 'Mr Robertson' };
let students = [
  { id: 1, studentName: 'David Jackson', age: 19, gpa: 3.4, classId: 1 },
  { id: 2, studentName: 'Peter Parker', age: 19, gpa: 2.9, classId: 1 },
  { id: 3, studentName: 'Robert Smith', age: 18, gpa: 3.1, classId: 1 },
  { id: 4, studentName: 'Rebecca Black', age: 19, gpa: 2.1, classId: 1 }

];
let oneStudent={ id: 1, studentName: 'David Jackson', age: 19, gpa: 3.4, classId: 1 };


  beforeEach(() => {
    const spy = jasmine.createSpyObj('ClassMockService', ['get']);

    TestBed.configureTestingModule({
      imports: [
        HomeModule,
        StoreModule.forRoot(reducers),
        EffectsModule.forRoot([])
      ],
      providers: [
        HomeEffects,

        provideMockActions(() => actions),
      ],
    });

    effects = TestBed.get(HomeEffects);
  });
  it('getClasses should work', () => {

    const action = new HomeActions.GetClassAction();
    const completion = new HomeActions.GetClassListAction(classes);

    actions = hot('--a-', { a: action });
    const expected = cold('--b', { b: completion });

    expect(effects.getClasses$).toBeObservable(expected);
  });
  it('editClasses should work', () => {

    const action = new HomeActions.EditClassAction(oneClass);
    const completion = new HomeActions.GetClassAction();

    actions = hot('--a-', { a: action });
    const expected = cold('--b', { b: completion });

    expect(effects.editClasses$).toBeObservable(expected);
  });

  it('addClasses should work', () => {

    const action = new HomeActions.AddClassAction(oneClass);
    const completion = new HomeActions.GetClassAction();

    actions = hot('--a-', { a: action });
    const expected = cold('--b', { b: completion });

    expect(effects.addClasses$).toBeObservable(expected);
  });
  it('deleteClasses should work', () => {

    const action = new HomeActions.DeleteClassAction(oneClass);
    const completion = new HomeActions.GetClassAction();

    actions = hot('--a-', { a: action });
    const expected = cold('--b', { b: completion });

    expect(effects.deleteClasses$).toBeObservable(expected);
  });

  it('getStudent should work', () => {

    const action = new HomeActions.GetStudentAction(1);
    const completion = new HomeActions.GetStudentListAction(students);

    actions = hot('--a-', { a: action });
    const expected = cold('--b', { b: completion });

    expect(effects.getStudent$).toBeObservable(expected);
  });
  it('addStudent should work', () => {

    const action = new HomeActions.AddStudentAction(oneStudent);
    const completion = new HomeActions.GetStudentAction(1);

    actions = hot('--a-', { a: action });
    const expected = cold('--b', { b: completion });

    expect(effects.addStudent$).toBeObservable(expected);
  });
  it('editStudent should work', () => {

    const action = new HomeActions.EditStudentAction(oneStudent);
    const completion = new HomeActions.GetStudentAction(1);

    actions = hot('--a-', { a: action });
    const expected = cold('--b', { b: completion });

    expect(effects.editStudent$).toBeObservable(expected);
  });
  it('deleteStudent should work', () => {

    const action = new HomeActions.DeleteStudentAction(oneStudent);
    const completion = new HomeActions.GetStudentAction(1);

    actions = hot('--a-', { a: action });
    const expected = cold('--b', { b: completion });

    expect(effects.deleteStudent$).toBeObservable(expected);
  });
});


