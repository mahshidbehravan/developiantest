import { TestBed, async } from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import { HomeComponent } from './home.component';
import { ShareModule } from '../share/share.module';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ClassComponent } from './class/class.component';
import { EditNewComponent } from './class/edit-new/edit-new.component';
import { StudentComponent } from './student/student.component';
import { StudentEditNewComponent } from './student/edit-new/edit-new.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule, Store } from '@ngrx/store';
import { HomeEffects } from './store/home.effect';
import { reducers } from './store/index';
import { ClassMockService } from '../services/class.service.mock';
import { StudentMockService } from '../services/student.mock';


describe('HomeComponent', () => {

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HomeComponent,
         ClassComponent,
         EditNewComponent,
          StudentComponent ,
          StudentEditNewComponent], imports:
           [ RouterTestingModule ,
             ShareModule ,
             NoopAnimationsModule,
             FormsModule,
             ReactiveFormsModule,
             FormsModule,
             ReactiveFormsModule,
             StoreModule.forRoot(reducers),
             EffectsModule.forRoot([])
             ],

    }).compileComponents();
  }));


  it('should create the home', async(() => {
    const fixture = TestBed.createComponent(HomeComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
});
