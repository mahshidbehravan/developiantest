import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { State, getclass } from './store';
import { GetClassAction } from './store/home.action';
import { ClassModel } from '../models/class';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public selectValue :ClassModel;
  constructor(private store: Store<State>) { }

  ngOnInit() {
    this.store.dispatch(new GetClassAction());

  }
  selectedClass(val){
this.selectValue = val;
  }

}
