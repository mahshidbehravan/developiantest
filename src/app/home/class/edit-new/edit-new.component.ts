import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-edit-new',
  templateUrl: './edit-new.component.html',
  styleUrls: ['./edit-new.component.css']
})
export class EditNewComponent implements OnInit {

  public form: FormGroup;
  public IsNew = true;
  private id: number;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
  private fb: FormBuilder,
  public thisDialogRef: MatDialogRef<EditNewComponent>) { }

  ngOnInit() {
    this.form = this.fb.group({
      className: ['', Validators.required],
      location: ['', Validators.required],
      teacher: ['', Validators.required],
    });

    this.IsNew = this.data.isNew;
    if (!this.data.isNew ) {
      this.form.controls.className.setValue(this.data.selectedRow.className);
      this.form.controls.location.setValue(  this.data.selectedRow.location );
      this.form.controls.teacher.setValue(this.data.selectedRow.teacher);
      this.id = this.data.selectedRow.id;
    } else {
      this.id = this.data.nextId;
    }

  }
  save() {
    const returnValue  = {
      id: this.id,
      className: this.form.controls.className.value,
      location: this.form.controls.location.value,
      teacher: this.form.controls.teacher.value,
    };
    this.thisDialogRef.close(returnValue);
  }
  close(){
    this.thisDialogRef.close();

  }

}
