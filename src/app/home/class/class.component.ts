import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Store } from '@ngrx/store';
import { State, getclass } from '../store';
import { GetClassAction, EditClassAction, AddClassAction, DeleteClassAction, SelectedClassAction } from '../store/home.action';
import { MatTableDataSource, MatDialog } from '@angular/material';
import { ClassModel } from 'src/app/models/class';
import { EditNewComponent } from './edit-new/edit-new.component';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-class',
  templateUrl: './class.component.html',
  styleUrls: ['./class.component.css'],
})
export class ClassComponent implements OnInit {

  public dataSource: ClassModel[] = [];
public selectedRowIndex:number;
  constructor(private store: Store<State>,
    public dialog: MatDialog,
    private router: Router,
    private route: ActivatedRoute
    ) { }

  ngOnInit() {

    this.store.select(getclass).subscribe(res => {
      if (res) {
        this.dataSource =  res;
      }
    });
  }
  Edit(selectedValue) {
    const dialogRef = this.dialog.open(EditNewComponent, {
      width: '43vw',
      data: {isNew: false , selectedRow: selectedValue}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.store.dispatch(new EditClassAction(result));
      }
    });

  }
  Add() {
    const dialogRef = this.dialog.open(EditNewComponent, {
      width: '43vw',
      data: {isNew: true , nextId: this.dataSource.length + 1 }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.store.dispatch(new AddClassAction(result));
      }
    });
  }

  Delete(selectedValue){
    this.store.dispatch(new DeleteClassAction(selectedValue));
  }
  clickRow(selectedValue ) {
    this.selectedRowIndex=selectedValue.id;
    this.store.dispatch(new SelectedClassAction(selectedValue));
    this.router.navigate(['./' , selectedValue.id , 'student' ], {relativeTo: this.route});
  }

}
