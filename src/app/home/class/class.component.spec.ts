import { TestBed, async } from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import { StoreModule, Store } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { ClassComponent } from './class.component';
import { reducers } from '../store/index';
import { ShareModule } from 'src/app/share/share.module';
import { ClassMockService } from 'src/app/services/class.service.mock';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { HomeComponent } from '../home.component';

class MockClassService {
 classes = [
    { id: 1, className: 'Biology', location: 'Building 3 Room 201', teacher: 'Mr Robertson' },
    { id: 2, className: 'English', location: 'Building 3 Room 134', teacher: 'Miss Sanderson' },
  ];
}
describe('ClassComponent', () => {

let component: ClassComponent;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ClassComponent],

      imports:[ StoreModule.forRoot(reducers),
        RouterTestingModule,
        ShareModule,
        EffectsModule.forRoot([])],
       providers: [ClassComponent , { provide: ClassMockService, useClass: MockClassService }]
    });

  }));


  it('should create the classe', async(() => {
    const fixture = TestBed.createComponent(ClassComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

});
