import { Component, OnInit, Input } from '@angular/core';
import { StudentModel } from 'src/app/models/student';
import { Store } from '@ngrx/store';
import { State, getclass, getstudent, getSelectedClass } from '../store';
import { MatDialog } from '@angular/material';
import { StudentEditNewComponent } from './edit-new/edit-new.component';
import { GetStudentAction, EditStudentAction, AddStudentAction, DeleteStudentAction } from '../store/home.action';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {

  dataSource: StudentModel[] = [];
  private classId: number;
  public ClassName: string;

  constructor(private store: Store<State>,
    public dialog: MatDialog,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params
      .subscribe(
        (params: Params) => {
          this.classId = +params['id'];
          this.store.dispatch(new GetStudentAction(this.classId));
          this.store.select(getstudent).subscribe(res => {
            if (res) {
              this.dataSource = res;
            }
          });
        }
      );
    this.store.select(getSelectedClass).subscribe(res => {

      if (res) {
        this.ClassName = res.className;
      }
    })

  }
  Edit(selectedValue) {
    const dialogRef = this.dialog.open(StudentEditNewComponent, {
      width: '43vw',
      data: { isNew: false, selectedRow: selectedValue, classId: this.classId }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.store.dispatch(new EditStudentAction(result));
      }
    });

  }
  Add() {
    const loalStorageItem = JSON.parse(localStorage.getItem('student'));

    const dialogRef = this.dialog.open(StudentEditNewComponent, {
      width: '43vw',
      data: { isNew: true, nextId: loalStorageItem.length + 1, classId: this.classId }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.store.dispatch(new AddStudentAction(result));
      }
    });
  }

  Delete(selectedValue) {
    this.store.dispatch(new DeleteStudentAction(selectedValue));
  }

}
