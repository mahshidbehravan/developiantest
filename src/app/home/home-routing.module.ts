import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home.component';
import { StudentComponent } from './student/student.component';

const routes: Routes = [
  {path: '', component : HomeComponent , children: [
{path : ':id/student' , component : StudentComponent}

  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
