
import { Injectable } from '@angular/core';
import { AbstractControl, ValidatorFn } from '@angular/forms';
import { StudentMockService } from '../services/student.mock';

@Injectable()
export class StudentValidationService {

  public checkName(mockService: StudentMockService, classId: number, IsNew: boolean): ValidatorFn {
  
    return (c: AbstractControl): { [key: string]: boolean } | null => {
      if (mockService.validStudent(c.value, classId) && IsNew) {
        return { 'isExs': true };
      }
      return null;
    };
  }

}




