import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { StudentModel } from '../models/student';

@Injectable()
export class StudentMockService {

  private students: StudentModel[] = [];
  private nextId: number;

  constructor() {
    const loalStorageItem = JSON.parse(localStorage.getItem('student'));
    this.students = [
      { id: 1, studentName: 'David Jackson', age: 19, gpa: 3.4, classId: 1 },
      { id: 2, studentName: 'Peter Parker', age: 19, gpa: 2.9, classId: 1 },
      { id: 3, studentName: 'Robert Smith', age: 18, gpa: 3.1, classId: 1 },
      { id: 4, studentName: 'Rebecca Black', age: 19, gpa: 2.1, classId: 1 },
      { id: 5, studentName: 'Mahshid Behravan', age: 18, gpa:10, classId: 2 },
      { id: 6, studentName: 'Mohammad Soofi', age: 19, gpa: 10, classId: 2 },

    ];
  }

  public getStudent(classId): Observable<StudentModel[]> {
    localStorage.setItem('student', JSON.stringify(this.students));
    return of(this.students.filter(x => x.classId == classId));

  }

  public edit(studentModel: StudentModel): Observable<any> {

    const updateItem = this.students.find(this.findIndexToUpdate, studentModel.id);
    const index = this.students.indexOf(updateItem);
    this.students[index] = studentModel;

    localStorage.removeItem('student');
    localStorage.setItem('student', JSON.stringify(this.students));
    return of(studentModel.classId);
  }

  public add(studentModel: StudentModel): Observable<any> {

    this.students.push(studentModel);
    localStorage.setItem('student', JSON.stringify(this.students));
    return of(studentModel.classId);
  }

  public delete(studentModel: StudentModel): Observable<any> {
    const index: number = this.students.indexOf(studentModel);
    if (index !== -1) {
      this.students.splice(index, 1);
    }
    localStorage.removeItem('student');
    localStorage.setItem('student', JSON.stringify(this.students));
    return of(studentModel.classId);
  }

  validStudent(name: string, classId: number) {
    return this.students.find(x => x.studentName.trim() === name.trim() && x.classId === classId);
  }
  findIndexToUpdate(newItem) {
    return newItem.id === this;
  }


}
