export class ClassModel {
    id: number;
    className: string;
    location: string;
    teacher: string;
}
